import requests
import json
import time

'''
Make a "safe" get request to the specified URL optionally using a premade Session object.
This request is safe in that it will attempt the get request up to four times if etiher:
    - the response is not a valid json object (example: 404 Not Found)
    - the response does not pass the validator method

:param link: the url at which the request should be made
:param session: optional requests.Session() object which allows caller to add headers and cookies, default is a simple requests.Session() object
:param validator: a method which takes the json parsed response object and returns True iff it is considered valid, default is a lambda which always returns True
:param retries: the number of previous attempts (this parameter should always be 0 unless called from within the makeGetRequest method)
:returns the json parsed response object or None if no valid response was found after 4 tries
'''
def makeGetRequest(link, session=requests.Session(), validator=(lambda _: True), retries=0):
    try:
        #make get request
        res = session.get(link)
        payload = json.loads(res.text) #parse response as a json object - if the response is not valid json this will cause an error and thus a retry
        if validator(payload): #test json object against validator - default validator always returns True
            return payload #this is a valid response so return json object
    except Exception:
        pass #do nothing

    # either encountered an exception or got an invalid return so try again
    if retries >= 4:
        return None
    else:
        if retries > 2:
            time.sleep(0.5) #pause briefly
        return makeGetRequest(link, session, validator, retries+1) #increment retries counter and try again