import subprocess

from sqlalchemy import create_engine, text
import sqlalchemy.exc
from sqlalchemy_utils import database_exists, create_database

class PostgreSQLDB:
    def __init__(self, dbname, create_command=None):
        self.engine = create_engine('postgresql+psycopg2:///' + dbname)
        if not database_exists(self.engine.url) and create_command is not None:
            subprocess.run(create_command)
        self.conn = self.engine.connect()
    def execute(self, query, **kwargs):
        try:
            result = self.conn.execute(text(query), kwargs)
        except sqlalchemy.exc.DatabaseError:
            conn = self.engine.connect()
            result = self.conn.execute(text(query), kwargs)
        return result
