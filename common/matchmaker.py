import csv
from abc import ABC, abstractmethod
from tqdm import tqdm
from nltk import sent_tokenize
from tabulate import tabulate
from sklearn.model_selection import train_test_split; 

#an abstract base class
class Matchmaker(ABC):
    def __init__(self):
        super().__init__()
    
    '''
    train model on a set of training examples

    :param trainingFileName: a csv of training examples where every row is of the form [claim1, claim2, true score (0 or 1)]
    '''
    @abstractmethod
    def trainModel(self, training_iterable):
        pass
    
    '''
    determine whether two claims match 

    :param claim1: the first claim
    :param claim2: the second claim
    :returns: a tuple of the form (claim1, claim2, confidence, score) where score is 1 if the claims are a match and 0 otherwise
    '''
    @abstractmethod
    def compareTwo(self, claim1, claim2):
        pass
    
    '''
    find all matches for a given sentence

    :param sentence: the sentence to be checked against the database
    :returns: a list of Match objects
    '''
    @abstractmethod
    def matchSentence_db(self, sentence):
        pass
    
    '''
    method called when pipeline is finished
    '''
    @abstractmethod
    def onFinish(self):
        pass

    '''
    calculate and print f1 scores for test data

    :param testFileName: a csv of test data where each row is of the form [difficulty, claim1, claim2, true score (0 or 1)]
    :param numDifficultyLevels: the number of distinct difficulty levels - difficulty ids should be 0,1,...,numDifficultyLevels-1
    '''
    def testModel(self, testFileName, numDifficultyLevels=2, random_states=[42], verbose=False):
        pbar=None #progress bar

        with open(testFileName, 'r',errors='ignore') as f:
            #read in rows from csv
            test_iterable=csv.reader(f, delimiter=',')
            data=list(test_iterable) #a list of lists???

            #initialize data collection variables
            totalTruePos,totalFalsePos,totalFalseNeg,totalTrueNeg=0,0,0,0 #keep track of totals across difficulty level
            results=[[[0 for _ in range(2)] for _ in range(2)] for _ in range(numDifficultyLevels)] #create a numDifficultyLevelsx2x2 3d array whose dimensions are difficulty, true score, and alg score respectively
            
            for random_state in random_states:
                train, test=train_test_split(data[1:], test_size=0.99, random_state=random_state) #type: pandas df
                print(len(test))
                #self.trainModel(train)

                #initialize progress bar if it hasn't been already
                if not pbar:
                    pbar = tqdm(total=len(test)*len(random_states), position=0, ncols=100, mininterval=1.0) #total is the product of the number of random states and the number of tests per trial

                #test model
                for row in test:
                    #extract relevent values
                    difficulty=int(row[0]) #integer representing the difficulty class of the training example
                    claim1=row[1]
                    claim2=row[2]
                    trueScore=int(row[3]) #0 or 1

                    #get algorithm result
                    tup=self.compareTwo(claim1,claim2)
                    algScore=int(tup[3]) #compareTwo returns a tuple of the form (claim1, claim2, confidence, isMatch) but only want isMatch
                    if verbose and algScore!=trueScore:
                        #got case wrong
                        print(tabulate([['difficulty',difficulty],['claim1',claim1],['claim2',claim2],['confidence',tup[2]],['true score',trueScore],['alg score',algScore]]))

                    #increment appropriate value
                    results[difficulty][trueScore][algScore]+=1

                    #update progress bar
                    pbar.update(1)

            #calculate f1 scores for each difficulty level
            for difficulty in range(numDifficultyLevels):
                #calculate f1 scores for difficulty level
                truePos=results[difficulty][1][1]  #true positives: true=1, alg=1
                falsePos=results[difficulty][0][1] #false positives: true=0, alg=1
                falseNeg=results[difficulty][1][0] #false negatives: true=1, alg=0
                trueNeg=results[difficulty][0][0]
                print('************************************** difficulty',difficulty,'**************************************')
                print(tabulate([['true 0',trueNeg,falsePos],['true 1',falseNeg,truePos]], headers=['alg 0','alg 1']))
                f1=self._calculateF1(truePos, falsePos, falseNeg)
                print('f1:\t{}'.format(f1))

                #increment totals
                totalTruePos+=truePos
                totalFalsePos+=falsePos
                totalFalseNeg+=falseNeg
                totalTrueNeg+=trueNeg
                
            #calculate overall f1 score
            print('************************************** overall **************************************')
            print(tabulate([['true 0',totalTrueNeg,totalFalsePos],['true 1',totalFalseNeg,totalTruePos]], headers=['alg 0','alg 1']))
            print('f1:\t{}'.format(self._calculateF1(totalTruePos, totalFalsePos, totalFalseNeg)))
            print(f'Accuracy: {100*(totalTruePos+totalTrueNeg)/(totalTrueNeg+totalTruePos+totalFalsePos+totalFalseNeg)}')

    '''
    match all sentences in a file against the database

    :param filename: name of the file to read from
    :returns: a list of tuples of the form (original sentence, matched sentence, match confidence)
    '''
    def matchFile_db(self, filename):
        #extract sentences from file
        with open(filename, 'r') as f:
            text=' '.join(f.readlines()) #replace newlines with spaces
            sentences=sent_tokenize(text) #use nltk to split into a list of sentences
        
        #initialize progress bar
        pbar = tqdm(total=len(sentences), position=0, ncols=100, mininterval=1.0)
                    
        #match each sentence against db
        results=[] #keep track of results
        for sentence in sentences:
            results+=self.matchSentence_db(sentence) #match sentence against db and add matches to result list
            pbar.update(1) #update progress bar
    
    #"private" helper methods
    '''
    calculate f1 score (https://en.wikipedia.org/wiki/F1_score)

    :param truePos: the number of true positive text cases
    :param falsePos: the number of false positive text cases
    :param falseNeg: the number of false negative text cases
    :returns: the f1 score for the given number of true postitive, false positive, and false negative counts
    '''
    def _calculateF1(self, truePos, falsePos, falseNeg):
        if (truePos+falsePos)==0 or (truePos+falseNeg)==0: 
            return -1
        precision=truePos/(truePos+falsePos)
        recall=truePos/(truePos+falseNeg)
        print('p:\t{}'.format(precision))
        print('r:\t{}'.format(recall))
        return 2*precision*recall/(precision+recall) #f1 score is harmonic mean of precision and recall