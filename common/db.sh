#!/bin/bash

# exit at the very first failing command
set -e
set -o pipefail

mypath=`realpath $0`
mybase=`dirname $mypath`
source $mybase/../setup/utils.bashrc

schema=claims
dumpfile=$schema-dump.sql.gz

function usage {
    echo "usage: $0 new|test|dump|restore"
    echo "  $db db must have already been created with facts schema!"
    echo "  new: initialize $schema schema with no data"
    echo "  test: initialize $schema schema with some test data"
    echo "  dump: dump data in $schema schema into $dumpfile"
    echo "  restore: restore $schema schema from $dumpfile"
    echo
}

if [[ $# -ne 1 ]]; then
    usage
    exit 1
fi

cd $mybase

if [[ "$1" == 'new' ]]; then
    psql $db -c "DROP SCHEMA IF EXISTS $schema CASCADE"
    psql $db -c "CREATE SCHEMA $schema"
    (echo "SET search_path TO $schema, facts, public;" && cat db/create.sql) | psql -f - $db
elif [[ "$1" == 'test' ]]; then
    psql $db -c "DROP SCHEMA IF EXISTS $schema CASCADE"
    psql $db -c "CREATE SCHEMA $schema"
    (echo "SET search_path TO $schema, facts, public;" && cat db/create.sql db/test-load.sql) | psql -f - $db
elif [[ "$1" == 'restore' ]]; then
    psql $db -c "DROP SCHEMA IF EXISTS $schema CASCADE"
    gunzip --stdout $dumpfile | psql -f - $db
elif [[ "$1" == 'dump' ]]; then
    pg_dump --no-owner --encoding=UTF8 -n $schema $db | gzip - > $dumpfile
else
    usage
    exit 1
fi
