import os
import requests
import pyaudio
import paho.mqtt.client as mqtt
from time import sleep
from queue import Queue
from threading import Thread
from nltk import sent_tokenize
from youtube_dl import YoutubeDL
from youtube_dl.utils import DownloadError

from Claim import Claim

import sys
from os import path
sys.path.insert(0, path.abspath(path.join(path.dirname(__file__),'..','common')))
from audioutils import videoToFlac

'''
collect audio from a online stream
based on https://github.com/travishathaway/python-icecast-record/blob/master/icerec/stream_writer.py
'''
class StreamCollector(Thread):
    def __init__(self, audioQueue, source):
        Thread.__init__(self) #call super constructor

        self.audioQueue=audioQueue #queue to fill

        #extract values corresponding to source
        self.server_url = source.value['url']

    def run(self):
        self.done=False #inidicate not done

        #make request with stream as true so it won't wait for all content to download
        request = requests.get(self.server_url, stream=True) 

        #get the bitrate from response headers
        self.bitrate=self.getBitRate(request.headers)

        for block in request.iter_content(int(self.bitrate/8*1000)): #we want to get audio one second at a time (there are 1000/8 bytes in a kilobit) 
            if self.done: #kill switch has been activiated
                break #break out of loop

            if block:
                self.audioQueue.put(block) #add block to outqueue

    '''
    get the audio bitrate (in kbps = kilo bits/second = 1000 bits/sec) of the audio stream based on the response headers
    assumes stream is an IceCast stream

    :param header: response headers from a requests.get() call
    :return: the bitrate of the audio stream
    '''
    def getBitRate(self, headers):
        #get the bitrate from the headers
        try:
            brString=headers['icy-br']
            if ',' in brString:
                brString=brString[:brString.index(',')]
            return int(brString)
        except:
            #failed to get bitrate
            raise RuntimeError('[MP] failed to get bitrate for {}'.format(self.source.name))

    '''
    Stop recording audio and stop thread
    '''
    def gracefulKill(self):
        self.done=True #indicate finished

'''
collect audio from device microphone
NOTE: this code will not work when run on a vm since the vm has no audio input
based on https://github.com/GoogleCloudPlatform/python-docs-samples/blob/master/speech/cloud-client/transcribe_streaming_mic.py
'''
class MicrophoneCollector(Thread):
    def __init__(self, audioQueue, rate):
        Thread.__init__(self) #call super constructor

        self.audioQueue=audioQueue #queue to fill
        self.rate=rate
    
    def run(self):
        #start recording
        audioInterface = pyaudio.PyAudio()
        audioStream = audioInterface.open(
            format=pyaudio.paInt16,             #want paInt16 to match with Linear16 (16 bit ints) 
            channels=1,                         #google currently only supports 1-channel (mono) audio (https://goo.gl/z757pE)
            rate=self.rate,                     #set frames per second
            input=True,                         #we want to collect input
            frames_per_buffer=self.rate,        #set blocksize so each block is one second
            stream_callback=self.fillQueue,     #method called when new audio is collected
        )

        #wait for kill switch
        self.done=False
        while not self.done:
            sleep(1)

        #stop recording
        audioStream.stop_stream()
        audioStream.close()
        audioInterface.terminate()
    
    '''
    continuously collect data from the audio stream, into the audioQueue
    this method is called by the pyaudio stream_callback  
    '''
    def fillQueue(self, inData, frameCount, timeInfo, statusFlags):
        self.audioQueue.put(inData)
        return None, pyaudio.paContinue #indicate to continue recording
    
    '''
    Stop recording audio and stop thread
    '''
    def gracefulKill(self):
        self.done=True #activate kill switch

'''
collect subtitles from bbc mqtt stream
this takes advantage of the mqtt bbc subtitle stream provided by an anonymous source. This could be extended to other mqtt streams
    but to our knowledge only bbc subtitle streams currently exist
based on https://github.com/eclipse/paho.mqtt.python and http://test.mosquitto.org/bbc/
'''
class BBCCollector: #note that this is not actually a thread since mqtt is already synchronous but rather pretends to be a thread so that no one can tell the difference
    def __init__(self, textQueue):
        self.textQueue=textQueue #queue to fill

        #setup mqtt
        self.client = mqtt.Client()
        self.client.on_connect = self.on_connect
        self.client.on_message = self.on_message

        self.firstSentence=True #flag indicating whether at least one sentence has been found to ensure that the intiital sentence fragment is ignored
        self.currentString='' #messages are appended to the end and completed sentences are removed from the beginning

    def start(self):
        self.client.connect("iot.eclipse.org")

        # non blocking call that processes network traffic, dispatches callbacks and handles reconnecting
        self.client.loop_start() #note that this is non-blocking

    # The callback for when the client receives a CONNACK response from the server.
    def on_connect(self, client, userdata, flags, rc):
        # Subscribing in on_connect() means that if we lose the connection and reconnect then subscriptions will be renewed.
        # self.client.subscribe("bbc/subtitles/bbc_one_london/raw")
        # self.client.subscribe("bbc/subtitles/bbc_two_england/raw")
        self.client.subscribe("bbc/subtitles/bbc_news24/raw")

    # The callback for when a PUBLISH message is received from the server.
    def on_message(self, client, userdata, msg):
        newContent=msg.payload.decode('utf-8') #convert from binary string to normal string
        newContent=newContent.replace('. . .', ' -').replace('i.e.', 'ie').replace('e.g.','eg') #manually deal with known nltk sentence tokenizer issues
       
        #make sure this isn't redundent input (sometimes words will be published individually and then later in a longer sentece fragment)
        if not self.currentString.endswith(newContent):
            #append newly found text
            self.currentString+=newContent
            #if we have more than one sentence, add finished sentences to the queue
            sents=sent_tokenize(self.currentString)
            if len(sents)>1: 
                #we have more than one sentence so add all but the last sentence to the queue
                for sent in sents[:-1]:
                    #ignore the initial sentence fragment resulting from opening the stream in the middle of a sentence
                    if not self.firstSentence:
                        #add finished sentence to the queue
                        self.textQueue.put(Claim(sent, sttConfidence=1)) #set sttConfidence to 1 since this is human generated (although there are still occasional errors)
                    else:
                        self.firstSentence=False
                self.currentString=sents[-1] #keep appending to the last sentence

    '''
    stop recieving from mqtt
    '''
    def gracefulKill(self):
        self.client.loop_stop()
    
    #implement methods that MatchmakerPipeline will call thinking this object is a thread
    def join(self):
        pass #do nothing

class YoutubeCollector(Thread):
    def __init__(self, youtubeIdQueue, fileQueue, rate):
        Thread.__init__(self) #call super constructor

        self.youtubeIdQueue=youtubeIdQueue
        self.fileQueue=fileQueue
        self.rate=rate

        self.resourcesPath=path.abspath(path.join(path.dirname(__file__),'resources')) #abs path to resources folder
        self.logger=self.Logger(self.resourcesPath) #initialize logger
        
        #setup youtubedl params
        self.ydl_opts = {
            #a full list of options can be found at #https://github.com/rg3/youtube-dl/blob/3e4cedf9e8cd3157df2457df7274d0c842421945/youtube_dl/YoutubeDL.py#L137-L312
            'quiet': True,                  #do not print to stdout
            'forcefilename': True,          #force youtubedl to send filename to logger
            'subtitleslangs': ['en'],       #only want english subtitles
            'writesubtitles': False,        #if subtitles have been uploaded for this video write them to file (set to true if you want subtitles)
            'writeautomaticsub': False,     #if no subtitles have been uploaded ask youtube for autogenerated subtitles (note sometimes yt cannot do this) (set to true if you want subtitles)
            'skip_download': False,         #set to true if only interested in subtitles
            'noplaylist': True,             #if accidentily passed a playlist ignore all but the first video
            'outtmpl': self.resourcesPath+'\\%(id)s.%(ext)s',    #output file names should be id of youtube video followed by relevent extensions (ex. https://youtu.be/[id] => [id].mp4 etc)
            'logger': self.logger,          #log output so that we can extract the filename
            'format': 'bestaudio/best'      #get best audio
        }

    def run(self):
        self.done=False
        while not self.done:
            ytid=self.youtubeIdQueue.get() #blocking get next ytid

            if ytid:
                #download video
                videoFile=self.downloadYoutubeVideo(ytid)

                #since both downloading video and extracting audio are slow, ensure that the kill switch hasn't been activated (this makes killing faster)
                if self.done:
                    break

                #convert to flac
                audioFile=videoToFlac(videoFile, self.resourcesPath, self.rate) #extract audio, convert to desired format, and put in audioFile
                os.remove(videoFile) #remove video file since no longer needed 

                self.fileQueue.put(audioFile) #add to queue

    def downloadYoutubeVideo(self, ytid):
        self.logger.lookFor(ytid) #indicate to look for the filename associated with this ytid

        with YoutubeDL(self.ydl_opts) as ydl:
            try:
                ydl.download(['https://youtu.be/'+ytid])
            except DownloadError:
                raise ValueError('[MP] unable to find youtube video '+ytid)
                
        if not self.logger.filename: #unable to find filename for some reason so raise exception
            raise Exception('[MP] unable to download youtube video '+ytid)

        return self.logger.filename   
            
    '''
    Gracefully and safely stop thread
    '''
    def gracefulKill(self):
        self.done=True #activate kill switch
        self.youtubeIdQueue.put(None) #release blocking
    
    '''
    simple class used to get the filename for downloaded youtube videos
    '''
    class Logger():
        def __init__(self, resourcesPath):
            self.resourcesPath=resourcesPath

        def lookFor(self,ytid):
            self.filename=None
            self.pathToFind=path.abspath(path.join(self.resourcesPath,ytid)) #seearch for resourcesPath/ytid

        def debug(self, msg):
            if msg.strip().startswith(self.pathToFind):
                self.filename=msg.strip()

        #only care about debug messages but still need to implement these methods to avoid throwing exception
        def warning(self, msg):
            pass
        def error(self, msg):
            pass