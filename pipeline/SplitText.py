from queue import Queue
from threading import Thread
from nltk import sent_tokenize

from Claim import Claim

'''
split text by sentence
'''
class TextSplitter(Thread):
    def __init__(self, textQueue, sentenceQueue):
        Thread.__init__(self) #call super constructor

        self.textQueue=textQueue    #read in queue
        self.sentenceQueue=sentenceQueue  #write out queue
    
        self.done=False #kill switch
        self.flushing=False #indicator of if currently flushing

    def run(self):
        while not self.done:
            text=self.textQueue.get() #get next element
            
            #check for flush
            if self.flushing:
                #consume queue until flush end indicator is reached
                while not text==self.FLUSH_END:
                    text=self.textQueue.get()
                self.flushing=False #indicate flush is done
                continue

            if text:
                for sentence in sent_tokenize(text): #use nltk to split into a list of sentences
                    if not self.flushing:
                        #put claim in write out queue
                        self.sentenceQueue.put(Claim(sentence, sttConfidence=1)) #since this is text input sttConfidence should be 1 (perfect text)

    '''
    gracefully and safely kill thread
    '''
    def gracefulKill(self):
        self.done=True
        self.textQueue.put(None) #release blocking
    
    '''
    flush queue
    '''
    def flush(self):
        self.flushing=True #indicate flush
        self.FLUSH_END='[FLUSH_END]' #constant put in the queue which indicates the end of the flush
        self.textQueue.put(self.FLUSH_END) #release blocking