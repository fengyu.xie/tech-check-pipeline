import os
from queue import Queue
from threading import Thread

import sys
from os import path
sys.path.insert(0, path.abspath(path.join(path.dirname(__file__),'..','common')))
from audioutils import getAudioLength, cropAudio

'''
split an audio stream into chunks which can be passed to the audio transcriber
this is necessary because google limits the number of seconds of consecuitive audio that can be transcribed
based on https://github.com/travishathaway/python-icecast-record/blob/master/icerec/stream_writer.py
'''
class StreamSplitter(Thread):
    def __init__(self, audioQueue, queueQueue, secondsLimit, secondsOffset):
        Thread.__init__(self) #call super constructor

        #store passed variables
        self.audioQueue=audioQueue #read in queue
        self.queueQueue=queueQueue  #write our queue (queue of queues)
        self.secondsLimit=secondsLimit #chunk length (maximum length of time google speech to text can handle)
        self.secondsOffset=secondsOffset #length of overlap between consecutive chunks
        #example: if secondsLimit=40 and secondsOffset=5 the start and end time of the chunks are 0-40,35-75,70-110,...
    
        self.queueList=[] #list of queues currently being written to
    
    def run(self):
        self.done=False #inidicate not done
        self.addQueue() #add first queue to be written to

        while not self.done: #until kill switch activated
            block=self.audioQueue.get()

            if block: #ensure block is not None
                for queueObj in self.queueList: #write block to each queue which has not yet reached the desired length
                    queueObj['secondsWritten']+=1

                    if queueObj['secondsWritten'] >= self.secondsLimit: 
                        #adding most recent chunk would exceed desired length so stop writing to this file
                        queueObj['queue'].put(None) #nonetype indicates end of queue
                        self.queueList.remove(queueObj) #remove queueObj from list so it will no longer be written to
                    else:
                        #have not yet reached desired length so write out block
                        queueObj['queue'].put(block)
                    
                    #when file has less than secondsOffset seconds remaining before it has reached the desired length, create a new queueObj and start writing to it
                    if not queueObj['hasCreatedNext'] and queueObj['secondsWritten'] >= self.secondsLimit-self.secondsOffset:
                        self.addQueue() #start recording data into the next queue
                        queueObj['hasCreatedNext']=True #make sure we don't create the next file again
        
        for queueObj in self.queueList:
            queueObj['queue'].put(None) #kill generators
    
    '''
    create a new queue dictionary object and add it to the queues list so that audio data will be written to it
    '''
    def addQueue(self):
        newQueue={
            'queue': Queue(), #start with an empty queue
            'secondsWritten': 0.0, #initially no bits have been written
            'hasCreatedNext': False #flag indicating if this file has spawned the file which will follow it sequentially (with some overlap)
        }
        self.queueList.append(newQueue)
        self.queueQueue.put(newQueue['queue']) 

    '''
    gracefully and safely kill thread
    '''
    def gracefulKill(self):
        self.done=True #activate kill switch
        self.audioQueue.put(None) #releasing blocking

class FileSplitter(Thread):
    def __init__(self, fileInQueue, fileOutQueue, secondsLimit, secondsOverlap):
        Thread.__init__(self) #call super constructor

        self.fileInQueue=fileInQueue
        self.fileOutQueue=fileOutQueue
        self.secondsLimit=secondsLimit
        self.secondsOverlap=secondsOverlap

        self.resourcesPath=path.abspath(path.join(path.dirname(__file__),'resources')) #abs path to resources folder
    
    def run(self):
        self.done=False
        while not self.done:
            filename=self.fileInQueue.get() #of the form (filename, length in seconds)

            if filename:
                #get length
                length=getAudioLength(filename, self.resourcesPath)

                #iterate through file and crop 
                #for example if secondsLimit=60 and overlap=10, generate files with (start time, end time) as follows:
                #   (0,60),(50,110),(100,160),...
                for startTime in range(0, int(length)+1, self.secondsLimit-self.secondsOverlap):
                    croppedFile=cropAudio(filename, self.resourcesPath, startTime, self.secondsLimit)
                    self.fileOutQueue.put(croppedFile)
                
                #done with original file so remove
                os.remove(filename)

    '''
    Gracefully and safely stop thread
    '''
    def gracefulKill(self):
        self.done=True #activate kill switch
        self.fileInQueue.put(None) #release blocking
