from queue import Queue
from threading import Thread
from timeit import default_timer as timer

#import necessary filters
from Source import Source
from Claim import Claim
from CollectAudio import StreamCollector, MicrophoneCollector, BBCCollector, YoutubeCollector
from ConvertAudio import AudioConverter
from SplitAudio import StreamSplitter, FileSplitter
from TranscribeAudio import StreamTranscriber, FileTranscriber
from FilterClaims import ClaimFilterer
from MatchClaims import ClaimMatcher
from SplitText import TextSplitter

import sys
from os import path
sys.path.insert(0, path.abspath(path.join(path.dirname(__file__),'..','engine','common')))
from matchmaker import Matchmaker

class MatchmakerPipeline(Thread):
    '''
    :param outQueue: claims are added to this queue after they are finished being processed. The queue passed should be empty.
        This is how the pipeline outputs claims. Using a queue allows the pipeline to stream output in real time
    :param source: the source of the claims. Should be an instance of the Source enum
    :param model: an instance of the Matchmaker abstract class, passing this as an argument means that switching out models does not require changing any pipeline code
    :param youtubeIdQueue: for Source.YOUTUBE, a queue of youtube video ids to transcribe (ex. https://youtu.be/ATFwMO9CebA => ATFwMO9CebA)
    :param textInQueue: for Source.TEXT, a queue of text to match against the database. This queue can be filled in real time
    :param transcriptionThreshold: the minimum Google speech to text confidence required for a claim to continue through the pipeline. On a scale from 0 to 1
    :param claimThreshold: the minimum ClaimBuster threshold required for a claim to continue through the pipeline. On a scale from 0 to 1
    :param returnAllResults: if true, claims which do not meet both the transcriptionThreshold and claimThreshold are returned, but not matched against the db.
        This is primarily used if the client wants the whole transcript. Currently used in the flask app demo
    '''
    def __init__(self, outQueue, source, model, youtubeIdQueue=None, textInQueue=None, transcriptionThreshold=0.8, claimThreshold=0.25, returnAllResults=True):
        Thread.__init__(self) #call super constructor

        #constants
        self.rate=16000         #number of times audio is sampled per second (gogole prefers 16000 Hz) 
        self.secondsLimit=60    #number of seconds of audio sent to each google api call
        self.secondsOffset=10  #overlap between consecuative google api sessions
        self.transcriptionThreshold=transcriptionThreshold #minimum google confidence
        self.claimThreshold=claimThreshold   #minimum claimbuster score

        #store variables passed
        self.source=source      #desired audio source
        self.outQueue=outQueue  #queue to fill
        self.model=model        #Matchmaker object for matching claims
        self.youtubeIdQueue=youtubeIdQueue  #queue of youtube ids for Source.YOUTUBE
        self.textInQueue=textInQueue    #queue of string for SOURCE.TEXT
        self.returnAllResults=returnAllResults  #if true, return results which don't meet thresholds

        #make sure input is valid
        self.validateInput()
        
        #pipeline depends on type of source
        if self.source is Source.MICROPHONE:
            #microphone
            self.initializeMicrophone()
        elif self.source is Source.YOUTUBE:
            #youtube
            self.initializeYoutube()
        elif self.source is Source.BBC:
            #bbc
            self.initializeBBC()
        elif self.source is Source.TEXT:
            #manually inputted text
            self.initializeText()
        else:
            #stream
            self.initializeStream()

    def run(self):
        self.done=False
        self.startTime=timer() #record start time

        #start threads
        for child in self.children:
            child.start()
        
        #wait for threads to finish
        for child in self.children:
            child.join()
        
        self.stopTime=timer() #record stop time
    
    '''
    validate input so that pipeline can fail gracefully and raise relevent exception if input is invalid
    '''
    def validateInput(self):
        #make sure valid source passed
        if not isinstance(self.source, Source):
            raise ValueError('[MP] invalid source - '+str(self.source))
        
        #if youtube source, ensure that a valid youtubeIdQueue was also passed
        if self.source is Source.YOUTUBE and not hasattr(self.youtubeIdQueue, 'get'):
            raise ValueError('[MP] invalid youtube id queue - expected queue, got '+str(type(self.youtubeIdQueue)))
        
        #if text source, ensure that a valid text in queue was passed
        if self.source is Source.TEXT and not hasattr(self.textInQueue, 'get'):
            raise ValueError('[MP] invalid text queue - expected queue, got '+str(type(self.textInQueue)))

        #make sure claimQueue has put method (due to ducktyping we do not really care the exact type as long as we can call put)
        if not hasattr(self.outQueue, 'put'):
            raise TypeError('[MP] outQueue invalid - expected queue, got '+str(type(self.outQueue)))
        
        #make sure model object is a subclass of Matchmaker abstract base class
        if not isinstance(self.model, Matchmaker):
            raise TypeError('[MP] model invalid - expected a subclass of Matchmaker, got '+str(type(self.model)))

    '''
    setup pipeline for microphone input
    '''
    def initializeMicrophone(self):
        #initialize queues
        self.audioQueue=Queue()
        self.queueQueue=Queue()
        self.textQueue=Queue()
        self.claimQueue=Queue()
        #outQueue already created

        #initialize children threads
        mc=MicrophoneCollector(self.audioQueue, self.rate)
        ss=StreamSplitter(self.audioQueue,self.queueQueue,self.secondsLimit,self.secondsOffset)
        st1=StreamTranscriber(self.queueQueue,self.textQueue,self.transcriptionThreshold,self.returnAllResults,self.rate) #need two stream transcribers
        st2=StreamTranscriber(self.queueQueue,self.textQueue,self.transcriptionThreshold,self.returnAllResults,self.rate) #need two stream transcribers
        cf=ClaimFilterer(self.textQueue,self.claimQueue,self.claimThreshold,self.returnAllResults)
        cm=ClaimMatcher(self.claimQueue,self.outQueue,self.model)
        self.children=[mc,ss,st1,st2,cf,cm] #put children in array for easy calling

    '''
    setup pipeline for stream input
    '''
    def initializeStream(self):
        #initialize queues
        self.audioQueue=Queue()
        self.convertedQueue=Queue()
        self.queueQueue=Queue()
        self.textQueue=Queue()
        self.claimQueue=Queue()
        #outQueue already created

        #initialize children threads
        sc=StreamCollector(self.audioQueue, self.source)
        ac=AudioConverter(self.audioQueue,self.convertedQueue,self.rate)
        ss=StreamSplitter(self.convertedQueue,self.queueQueue,self.secondsLimit,self.secondsOffset)
        st1=StreamTranscriber(self.queueQueue,self.textQueue,self.transcriptionThreshold,self.returnAllResults,self.rate) #need two stream transcribers
        st2=StreamTranscriber(self.queueQueue,self.textQueue,self.transcriptionThreshold,self.returnAllResults,self.rate) #need two stream transcribers
        cf=ClaimFilterer(self.textQueue,self.claimQueue,self.claimThreshold,self.returnAllResults)
        cm=ClaimMatcher(self.claimQueue,self.outQueue,self.model)
        self.children=[sc,ac,ss,st1,st2,cf,cm] #put children in array for easy calling
    
    '''
    setup pipeline for youtube input
    '''
    def initializeYoutube(self):
        #initialize queues
        #youtubeIdQueue already created
        self.fileQueue=Queue()
        self.croppedQueue=Queue()
        self.textQueue=Queue()
        self.claimQueue=Queue()
        #outQueue already created

        #initialize children threads
        yc=YoutubeCollector(self.youtubeIdQueue,self.fileQueue,self.rate)
        fs=FileSplitter(self.fileQueue,self.croppedQueue,self.secondsLimit,self.secondsOffset)
        ft=FileTranscriber(self.croppedQueue,self.textQueue,self.transcriptionThreshold,self.returnAllResults)
        cf=ClaimFilterer(self.textQueue,self.claimQueue,self.claimThreshold,self.returnAllResults)
        cm=ClaimMatcher(self.claimQueue,self.outQueue,self.model)
        self.children=[yc,fs,ft,cf,cm] #put children in array for easy calling
    
    '''
    setup pipeline for bbc input
    '''
    def initializeBBC(self):
        #initialize queues
        self.textQueue=Queue()
        self.claimQueue=Queue()
        #outQueue already created

        #initialize children threads
        bbcc=BBCCollector(self.textQueue)
        cf=ClaimFilterer(self.textQueue,self.claimQueue,self.claimThreshold,self.returnAllResults)
        cm=ClaimMatcher(self.claimQueue,self.outQueue,self.model)
        self.children=[bbcc,cf,cm] #put children in array for easy calling

    '''
    setup pipeline for text input
    '''
    def initializeText(self):
        #initialize queues
        #textInQueue already created
        self.sentenceQueue=Queue()
        self.claimQueue=Queue()
        #outQueue already created

        ts=TextSplitter(self.textInQueue,self.sentenceQueue)
        cf=ClaimFilterer(self.sentenceQueue,self.claimQueue,self.claimThreshold,self.returnAllResults)
        cm=ClaimMatcher(self.claimQueue,self.outQueue,self.model)
        self.children=[ts,cf,cm]
    
    '''
    gracefully and safely kill pipeline but gracefully killing all children
    '''
    def gracefulKill(self):
        #gracefully kill children threads in order
        for child in self.children:
            child.gracefulKill()
        #wait for threads to finish
        for child in self.children:
            child.join()

        #finally, kill self
        self.done=True
    
    '''
    Attempt to flush pipeline of all input it is currently processing
    This method is only applicable for text source since in this case each user has their own MatchmakerPipeline object
    This is most commonly used when a user submits a new piece of text without waiting for the previously submitted text to finish
        in all other cases you do not know who else might be listening to the output from the MatchmakerPipeline object
    NOTE: this method cannot gaurantee that no unwanted output will escape the pipeline so if such a guarantee is required the caller will have to do it themself
    '''
    def flush(self):
        #flushing is only applicable for text sources
        if self.source is Source.TEXT:
            #consume current output queue
            while not self.outQueue.empty():
                self.outQueue.get(block=False)

            #tell all children to flush
            for child in reversed(self.children): #want to go through pipeline from end to beginning
                if hasattr(child,'flush'): #for safety reasons only call flush if child has a flush method
                    child.flush()