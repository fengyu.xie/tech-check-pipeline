#splits the audio file into separate snippets based on phrases
#converts each of the audio snippets into a text sentence
#stores the sentences into an array

# input = audio file (temporarily only .wav); output = array of strings

from pydub import AudioSegment
from pydub.silence import split_on_silence
import speech_recognition as sr

sound = AudioSegment.from_wav("test.wav")
r = sr.Recognizer()
sentences = []

chunks = split_on_silence(sound, min_silence_len=200, silence_thresh= -16)

for chunk in chunks:
    with sr.AudioFile(chunk) as source:
        audio = r.listen(chunk)
        sentence = r.recognize_google(chunk)
        if sentence not in sentences:
            sentences.append(sentence)
